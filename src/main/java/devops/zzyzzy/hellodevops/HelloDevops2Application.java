package devops.zzyzzy.hellodevops;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HelloDevops2Application {

	public static void main(String[] args) {
		SpringApplication.run(HelloDevops2Application.class, args);
	}

}
